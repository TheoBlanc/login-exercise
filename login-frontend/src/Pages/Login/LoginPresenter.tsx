import React from "react";
import { LoginWrapper, LoginContainer, FlexD } from "./LoginPresenterStyles";

import Input from "../../Components/Input";

const LoginPresenter = () => {
  return (
    <LoginWrapper>
      <LoginContainer>
        <Input type="text" required={true} placeholder="아이디" />
        <Input type="text" required={true} placeholder="비밀번호" />
        <FlexD>
          <button>로그인</button>
          <a href="#">아이디 찾기</a>|<a href="#">비밀번호 찾기</a>|
          <a href="#">회원가입 찾기</a>
          <button>페이스북 로그인</button>
          <button>구글 로그인</button>
        </FlexD>
      </LoginContainer>
    </LoginWrapper>
  );
};

export default LoginPresenter;
