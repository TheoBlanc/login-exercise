import styled from "styled-components";

export const LoginWrapper = styled.div`
  background-color: #fafafa;
  min-height: 80vh;
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const LoginContainer = styled.div`
  background-color: #ffffff;
  max-width: 350px;
  padding: 40px;
  padding-bottom: 30px;
  margin-bottom: 15px;
  form {
    width: 100%;
    button {
      margin: 10px;
    }
  }
`;

export const FlexD = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;
