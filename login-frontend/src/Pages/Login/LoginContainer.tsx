import React from "react";
import LoginPresenter from "./LoginPresenter";

interface Iprop {}

const LoginContainer: React.FC<Iprop> = () => {
  return <LoginPresenter />;
};

export default LoginContainer;
