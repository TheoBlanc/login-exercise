import React from "react";
import styled from "styled-components";
import Login from "./Pages/Login";
import GolbalStyle from "./Styles/Global/GolbalStyle";

const Wrap = styled.div`
  margin: 0 auto;
`;

const App: React.FC = () => {
  return (
    <div>
      <GolbalStyle />
      <Wrap>
        <Login />
      </Wrap>
    </div>
  );
};

export default App;
