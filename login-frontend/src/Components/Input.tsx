import React from "react";
import styled from "styled-components";

interface IProps {
  placeholder?: string;
  required?: boolean;
  value?: string;
  onChange?: VoidFunction;
  type?: string;
  className?: string;
}

const Container = styled.input`
  border: 0;
  background-color: #ffffff;
  height: 35px;
  font-size: 12px;
  padding: 0px 15;
`;

const Input: React.FC<IProps> = ({
  placeholder,
  required,
  value,
  onChange,
  type,
  className
}) => (
  <Container
    placeholder={placeholder}
    required={required}
    value={value}
    onChange={onChange}
    type={type}
    className={className}
  ></Container>
);

export default Input;
